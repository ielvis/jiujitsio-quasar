import { LocalStorage } from "quasar";
import { showErrorMessage } from "src/functions/function-show-error-message";

export default ({ router }) => {
  router.beforeEach((to, from, next) => {
    let loggedIn = LocalStorage.getItem("loggedIn");

    if (to.path === "/devJournal") {
      next();
    } else if (!loggedIn && to.path === "/") {
      // NOTE: '.replace' gets rid of user history so they can't go back
      router.replace("/auth").catch(err => {});
    } else if (!loggedIn && to.path !== "/auth") {
      showErrorMessage("Must be logged in to visit that page");
      // NOTE: '.replace' gets rid of user history so they can't go back
      router.replace("/auth").catch(err => {});
    } else {
      next();
    }
  });
};
