// Need to import Vue to use Vue methods to delete
import Vue from "vue";
import { uid } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "../functions/function-show-error-message";
import { Notify } from "quasar";

const state = {
  gyms: {
    //   "ID1": {
    //   name: "B Open Mat Sunday",
    //   type: "Gi",
    //   general_notes: "Was fun.",
    //   name: "a"
    // },
    // "ID2": {
    //   name: "A Open Mat Saturday",
    //   type: "NoGi",
    //   general_notes: "Was TEst.",
    //   name: "b"
    // }
  },
  search: "",
  sort: "name",
  show: "all",
  gymsDownloaded: false,
  direction: "ascending"
};

// Cannot be asynchronous
const mutations = {
  // Use Object.assign to target a property and apply the payload updates.
  updateGym(state, payload) {
    // NOTE: Taking care of payload.updates on the Firestore method
    Object.assign(state.gyms[payload.id], payload);
  },
  deleteGym(state, id) {
    Vue.delete(state.gyms, id);
  },
  addGym(state, payload) {
    Vue.set(state.gyms, payload.id, payload);
  },
  clearGyms(state) {
    state.gyms = {};
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setDirection(state, value) {
    state.direction = value;
  },
  setShow(state, value) {
    state.show = value;
  },
  setGymsDownloaded(state, value) {
    state.gymsDownloaded = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  updateGym({ dispatch }, payload) {
    dispatch("firebaseUpdateGym", payload);
  },
  // NOTE: Pass a commit method and the payload(ID of the gym in this case)
  // NOTE: to the action method, commit the payload to a mutation to change state.
  deleteGym({ dispatch }, id) {
    dispatch("firebaseDeleteGym", id);
  },
  // NOTE: Now that the Firestore has been added we can now dispatch an action to update the
  // NOTE: Firestore database. Our listener will update the state automatically
  // addGym({ commit }, gym) {
  addGym({ dispatch }, gym) {
    // NOTE, id being set in parent training session
    // let gymId = uid();
    // gym.id = gymId;
    let payload = gym;
    // commit("addGym", payload);
    dispatch("firebaseAddGym", payload);
  },
  setSearch({ commit }, value) {
    commit("setSearch", value);
  },
  setSort({ commit }, value) {
    commit("setSort", value);
  },
  setDirection({ commit }, value) {
    commit("setDirection", value);
  },
  setShow({ commit }, value) {
    commit("setShow", value);
  },
  firebaseReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("gyms")
      .get()
      .then(response => {
        commit("setGymsDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    // NOTE: Training Session Added, Edited or Removed 🤯
    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("gyms")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();

          if (change.type === "added") {
            commit("addGym", payload);
          } else if (change.type === "modified") {
            commit("updateGym", payload);
          } else if (change.type === "removed") {
            commit("deleteGym", payload.id);
          }
        });
      });
  },
  firebaseAddGym({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("gyms")
      .doc(payload.id)
      .set(payload)
      .then(function() {
        Notify.create("Gym added!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  firebaseUpdateGym({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    var gymRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("gyms")
      .doc(payload.id);

    gymRef
      .update(payload.updates)
      .then(function() {
        Notify.create("Gym updated!");
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        showErrorMessage(error.message);
      });
  },
  firebaseDeleteGym({}, gymId) {
    let userId = firebaseAuth.currentUser.uid;

    var gymRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("gyms")
      .doc(gymId);

    gymRef
      .delete()
      .then(function() {
        Notify.create("Gym deleted!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  }
};

const getters = {
  gymsSorted: state => {
    let gymsSorted = {};
    let keysOrdered = Object.keys(state.gyms);

    console.log("state.direction is: ", state.direction);
    console.log("state.sort is: ", state.sort);

    keysOrdered.sort((a, b) => {
      let gymAProp = state.gyms[a][
        state.sort
      ].toLowerCase();
      let gymBProp = state.gyms[b][
        state.sort
      ].toLowerCase();

      // Check the added property 'direction' for 'newest' or 'direction' and if true direction the list.
      if (state.sort === "name" && state.direction === "ascending") {
        if (gymAProp > gymBProp) return 1;
        else if (gymAProp < gymBProp) return -1;
        else return 0;
      } 
    });

    keysOrdered.forEach(key => {
      gymsSorted[key] = state.gyms[key];
    });

    return gymsSorted;
  },
  gymsFiltered: (state, getters) => {
    let gymsSorted = getters.gymsSorted;
    let gymsFiltered = {};
    if (state.search) {
      Object.keys(gymsSorted).forEach(function(key) {
        let gym = gymsSorted[key];
        let gymLowerCase = gym.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (gymLowerCase.includes(searchLowerCase)) {
          gymsFiltered[key] = gym;
        }
      });
      return gymsFiltered;
    }
    return gymsSorted;
  },
  allGyms: (state, getters) => {
    let gymsFiltered = getters.gymsFiltered;
    let gyms = {};
    Object.keys(gymsFiltered).forEach(function(key) {
      let gym = gymsFiltered[key];
      gyms[key] = gym;
    });
    return gyms;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
