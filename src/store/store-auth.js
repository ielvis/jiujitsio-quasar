import { firebaseAuth } from "boot/firebase";
import { LocalStorage, Loading } from "quasar";
import { showErrorMessage } from "src/functions/function-show-error-message";

const state = {
  loggedIn: false
};

// NOTE: Cannot be asynchronous
const mutations = {
  setLoggedIn(state, value) {
    state.loggedIn = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  registerUser({}, payload) {
    Loading.show();
    firebaseAuth
      .createUserWithEmailAndPassword(payload.email, payload.password)
      .then(response => {})
      .catch(error => {
        showErrorMessage(error.message);
      });
  },
  loginUser({}, payload) {
    Loading.show();
    firebaseAuth
      .signInWithEmailAndPassword(payload.email, payload.password)
      .then(response => {})
      .catch(error => {
        showErrorMessage(error.message);
      });
  },
  logoutUser() {
    firebaseAuth.signOut();
  },
  handleAuthStateChanged({ commit, dispatch }) {
    // TODO: FIX THIS FOR THE BLOG
    firebaseAuth.onAuthStateChanged(user => {
      // NOTE: Fixes Vue NavigationDuplicated
      Loading.hide();
      if (user) {
        // NOTE: User is signed in
        commit("setLoggedIn", true);
        LocalStorage.set("loggedIn", true);
        const path = "/";
        if (this.$router.path !== path) {
          this.$router.push(path);
        }
        // NOTE: when using an action that's not local:
        // NOTE: params are: route, payload & options
        // NOTE: 'root:true' lets us access from another module.
        dispatch("training_sessions/firebaseReadData", null, { root: true });
        dispatch("techniques/firebaseReadData", null, { root: true });
        dispatch("scenarios_and_transitions/firebaseReadData", null, {
          root: true
        });
        dispatch("partners/firebaseReadData", null, {
          root: true
        });
        dispatch("gyms/firebaseReadData", null, {
          root: true
        });
        dispatch("instructors/firebaseReadData", null, {
          root: true
        });
      } else {
        commit("training_sessions/clearTrainingSessions", null, { root: true });
        // commit("scenarios-and-transitions/clearScenarios", null, {
        //   root: true
        // });
        // commit("scenarios-and-transitions/clearTransitions", null, {
        //   root: true
        // });
        commit("training_sessions/setTrainingSessionsDownloaded", false, {
          root: true
        });
        // commit("scenarios-and-transitions/setScenariosDownloaded", false, {
        //   root: true
        // });
        // commit("scenarios-and-transitions/setTransitionsDownloaded", false, {
        //   root: true
        // });
        commit("setLoggedIn", false);
        LocalStorage.set("loggedIn", false);
      }
    });
  }
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
