import * as ctx from "../../../../quasar.conf.js";

describe.skip("Landing", () => {
  beforeEach(() => {
    cy.visit("/#/auth");
  });
  it(".should() - assert that <title> is correct", () => {
    cy.title().should("include", "jiujitsio");
  });
});

describe.skip("Auth tests", () => {
  before(() => {
    cy.clearCookies();
    cy.clearLocalStorage()  // clear all local storage
  });

  after(() => {
    cy.get("[data-cy=logout]").click();
    cy.clearCookies();
    cy.clearLocalStorage()  // clear all local storage
  })

  it("goes to /auth - ", () => {
    cy.visit("/#/auth");
    cy.get(".page-auth");
  });

  it("sets localStorage when logging in via form submission", () => {
    cy.get("input[name=email]").type(Cypress.env('TEST_email'));
    cy.get("input[name=password]").type(Cypress.env('TEST_password'));
    cy.get("[data-cy=submit]").click();


    // TODO: Make this go to /training_sessions and update all routes
    // NOTE: we should be redirected to /
    cy.url().should("include", "/");

    // NOTE: localStorage item "loggedIn" should be true or Quasar's version of it
    cy.url().should(() => {
      expect(localStorage.getItem("loggedIn")).to.eq("__q_bool|1");
    });
  });
});
