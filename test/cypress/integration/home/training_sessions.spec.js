import * as ctx from "../../../../quasar.conf.js";


describe("Adds a Training Session Correctly", () => {

  before(() => {
    cy.login();
  });

  after(() => {
    cy.get("[data-cy=logout]").click();
    cy.clearCookies();
    cy.clearLocalStorage()  // clear all local storage
  })

  it("shows the 'Add Training Session' modal", () => {
    // Note: {force: true} in case the Drift widget is overlapping
    cy.get("[data-cy=addTrainingSession]").click({force: true});
    cy.get("[data-cy=addTrainingSessionModal]");
  });

  it("Requires a title", () => {
    // Note: "Field is required" appears after attempting to submit w/o a title. 
    cy.get('[data-cy=modalTitle]').should('not.contain', 'Field is required')
    cy.get("[data-cy=confirm-modal-button]").click({force: true});
    cy.get('[data-cy=modalTitle]').should('contain', 'Field is required')
  });

  describe("Opens and closes 'Edit Training Session' modal", () => {
    before(() => {
      cy.login();
    });
  
    after(() => {
      cy.get("[data-cy=logout]").click();
      cy.clearCookies();
      cy.clearLocalStorage()  // clear all local storage
    })
  
    it("shows the 'Edit Training Session' modal after clicking on the pencil icon", () => {
      // Note: {force: true} in case the Drift widget is overlapping
      cy.get("[data-cy=edit-training-session-button]").click({force: true});
      cy.get("[data-cy=edit-training-session-card]");
    });
  
  
    it("closes the 'Edit Training Session' modal", () => {
      cy.get("[data-cy=modal-header-close-button]").click();
      cy.get("[data-cy=edit-training-session-card]").should('not.exist')
    });
  });

  // Note: Enter today's datetime as the title and attempts to create a new training session w/o a tech and w/o a sparring session. 
  it.skip("Adds a training session w/o tech and w/o sparring", () => {
    let trainingSessionTitle = Date.now()
    trainingSessionTitle = trainingSessionTitle.toString();

    // Note: Initially the modal should not have today's datetime string
    cy.get("[data-cy=modal-title]").should('not.have.value', trainingSessionTitle);

    // Note: Enter today's datetime as the title and them check for it on the modal
    cy.get("[data-cy=gi-icon]").click();
    cy.get("[data-cy=modal-title]").type(trainingSessionTitle);
    cy.get("[data-cy=modal-title]").should('have.value', trainingSessionTitle);

    // Note: Training session shouldn't exist on the main page until 'Save' is clicked. 
    cy.get("[data-cy=training-sessions-scroll-area]").should('not.contain', trainingSessionTitle)
    cy.get("[data-cy=confirm-modal-button]").click({force: true});
    cy.get("[data-cy=training-sessions-scroll-area]").should('contain', trainingSessionTitle)
  });

  it.skip("Adds a training session w tech and w/o sparring", () => {
    let trainingSessionTitle = Date.now()
    trainingSessionTitle = trainingSessionTitle.toString();

    // Note: Initially the modal should not have today's datetime string
    cy.get("[data-cy=modal-title]").should('not.have.value', trainingSessionTitle);

    // Note: Enter today's datetime as the title and them check for it on the modal
    cy.get("[data-cy=gi-icon]").click();
    cy.get("[data-cy=modal-title]").type(trainingSessionTitle);
    cy.get("[data-cy=modal-title]").should('have.value', trainingSessionTitle);

    // Note: Training session shouldn't exist on the main page until 'Save' is clicked. 
    cy.get("[data-cy=training-sessions-scroll-area]").should('not.contain', trainingSessionTitle)




    // cy.get("[data-cy=confirm-modal-button]").click({force: true});
    // cy.get("[data-cy=training-sessions-scroll-area]").should('contain', trainingSessionTitle)
  });


  it("closes the 'Add Training Session' modal", () => {
    cy.get("[data-cy=addTrainingSession]").click({force: true});
    cy.get("[data-cy=addTrainingSessionModal]");
    cy.get("[data-cy=modal-header-close-button]").click();
    cy.get("[data-cy=addTrainingSessionModal]").should('not.exist')
  });
});



describe.skip("'Edit Technique' modal works correctly", () => {
  before(() => {
    cy.login();
  });

  after(() => {
    cy.get("[data-cy=edit-technique-card]").within(() => {
      cy.get("[data-cy=modal-header-close-button]").click();
    });

    // Note: close the modal
    cy.get("[data-cy=edit-training-session-card]").within(() => {
      cy.get("[data-cy=modal-header-close-button]").click();
    });
    cy.get("[data-cy=edit-training-session-card]").should('not.exist')

    cy.get("[data-cy=logout]").click();
    cy.clearCookies();
    cy.clearLocalStorage()  // clear all local storage
  })

  it("shows the 'Edit Technique' modal after clicking on 'Edit Technique' from the 'Edit Training Session' modal", () => {
    // Note: {force: true} in case the Drift widget is overlapping
    cy.get("[data-cy=edit-training-session-button]").click({force: true});
    cy.get("[data-cy=edit-training-session-card]");

    cy.get("[data-cy=technique-title]").click();
    cy.get("[data-cy=edit-technique-button]").click();

    cy.get("[data-cy=edit-technique-card]")
  });
});



