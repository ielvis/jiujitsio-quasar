// Firebase App (the core Firebase SDK) is always required and must be listed first
import * as firebase from "firebase/app";

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";

var firebaseConfig = {
  apiKey: "AIzaSyDASOxEV6kotRWdqD7FgoE3K24juXxwiyY",
  authDomain: "jiujitsio.firebaseapp.com",
  databaseURL: "https://jiujitsio.firebaseio.com",
  projectId: "jiujitsio",
  storageBucket: "jiujitsio.appspot.com",
  messagingSenderId: "707575466273",
  appId: "1:707575466273:web:8162a3c7a2945167611660",
  measurementId: "G-PS5QTH95EL"
};
// Initialize Firebase
let firebaseApp = firebase.initializeApp(firebaseConfig);
let firebaseGoogleProvider = new firebase.auth.GoogleAuthProvider();
let firebaseAuth = firebaseApp.auth();
firebase.analytics();
let firebaseDb = firebase.firestore(firebaseApp);

export { firebaseAuth, firebaseDb, firebaseGoogleProvider };
