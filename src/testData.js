const firebase = require("firebase");
// Required for side-effects
require("firebase/firestore");

// Initialize Cloud Firestore through Firebase
firebase.initializeApp({
  apiKey: "AIzaSyDASOxEV6kotRWdqD7FgoE3K24juXxwiyY",
  authDomain: "jiujitsio.firebaseapp.com",
  projectId: "jiujitsio"
});

var db = firebase.firestore();

var testUserId = "oLwKHrIPFgRInTMywJJjiXcy0de2";

var training_sessions = [
  {
    datetimeEnd: "",
    datetimeStart: "a",
    detailed_sparring_notes: [],
    detailed_tech_notes: [],
    general_notes: "Was fun.",
    gym: "",
    id: "ID1",
    instructor: "",
    title: "B Open Mat Sunday",
    training_partner: "",
    type: "Gi"
  },
  {
    datetimeEnd: "",
    datetimeStart: "b",
    detailed_sparring_notes: [],
    detailed_tech_notes: [],
    general_notes: "Was TEst.",
    gym: "",
    id: "ID2",
    instructor: "",
    title: "A Open Mat Saturday",
    training_partner: "",
    type: "No-Gi"
  }
];

// NOTE: Add a new document in collection "users"
training_sessions.forEach(session => {
  db.collection("users")
    // NOTE: UserId at which to enter the training sessions
    .doc(testUserId)
    .collection("training_sessions")
    .doc(session.id)
    .set({
      ...session
    })
    .then(function(docRef) {
      console.log("Document written with ID: ", docRef);
    })
    .catch(function(error) {
      console.error("Error adding document: ", error);
    });
});
