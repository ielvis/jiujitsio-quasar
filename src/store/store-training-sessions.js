// Need to import Vue to use Vue methods to delete
import Vue from "vue";
import { uid } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "../functions/function-show-error-message";
import { Notify } from "quasar";

const state = {
  training_sessions: {
    //   "ID1": {
    //   title: "B Open Mat Sunday",
    //   type: "Gi",
    //   general_notes: "Was fun.",
    //   date: "a"
    // },
    // "ID2": {
    //   title: "A Open Mat Saturday",
    //   type: "NoGi",
    //   general_notes: "Was TEst.",
    //   date: "b"
    // }
  },
  search: "",
  sort: "date",
  show: "all",
  trainingSessionsDownloaded: false,
  direction: "ascending"
};

// Cannot be asynchronous
const mutations = {
  // Use Object.assign to target a property and apply the payload updates.
  updateTrainingSession(state, payload) {
    // NOTE: Taking care of payload.updates on the Firestore method
    Object.assign(state.training_sessions[payload.id], payload);
  },
  deleteTrainingSession(state, id) {
    Vue.delete(state.training_sessions, id);
  },
  addTrainingSession(state, payload) {
    Vue.set(state.training_sessions, payload.id, payload);
  },
  clearTrainingSessions(state) {
    state.training_sessions = {};
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setDirection(state, value) {
    state.direction = value;
  },
  setShow(state, value) {
    state.show = value;
  },
  setTrainingSessionsDownloaded(state, value) {
    state.trainingSessionsDownloaded = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  updateTrainingSession({ dispatch }, payload) {
    dispatch("firebaseUpdateTrainingSession", payload);
  },
  // NOTE: Pass a commit method and the payload(ID of the training_session in this case)
  // NOTE: to the action method, commit the payload to a mutation to change state.
  deleteTrainingSession({ dispatch }, id) {
    dispatch("firebaseDeleteTrainingSession", id);
  },
  // NOTE: Now that the Firestore has been added we can now dispatch an action to update the
  // NOTE: Firestore database. Our listener will update the state automatically
  // addTrainingSession({ commit }, training_session) {
  addTrainingSession({ dispatch }, training_session) {
    let trainingSessionId = uid();
    training_session.id = trainingSessionId;
    let payload = training_session;
    // commit("addTrainingSession", payload);
    dispatch("firebaseAddTrainingSession", payload);
  },
  setSearch({ commit }, value) {
    commit("setSearch", value);
  },
  setSort({ commit }, value) {
    commit("setSort", value);
  },
  setDirection({ commit }, value) {
    commit("setDirection", value);
  },
  setShow({ commit }, value) {
    commit("setShow", value);
  },
  firebaseReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("training_sessions")
      .get()
      .then(response => {
        commit("setTrainingSessionsDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    // NOTE: Training Session Added, Edited or Removed 🤯
    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("training_sessions")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();

          if (change.type === "added") {
            commit("addTrainingSession", payload);
          } else if (change.type === "modified") {
            commit("updateTrainingSession", payload);
          } else if (change.type === "removed") {
            commit("deleteTrainingSession", payload.id);
          }
        });
      });
  },
  firebaseAddTrainingSession({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("training_sessions")
      .doc(payload.id)
      .set(payload)
      .then(function() {
        Notify.create("Training session added!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  firebaseUpdateTrainingSession({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    var trainingSessionRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("training_sessions")
      .doc(payload.id);

    trainingSessionRef
      .update(payload.updates)
      .then(function() {
        Notify.create("Training session updated!");
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        showErrorMessage(error.message);
      });
  },
  firebaseDeleteTrainingSession({}, trainingSessionId) {
    let userId = firebaseAuth.currentUser.uid;

    var trainingSessionRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("training_sessions")
      .doc(trainingSessionId);

    trainingSessionRef
      .delete()
      .then(function() {
        Notify.create("Training session deleted!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  }
};

const getters = {
  trainingSessionsSorted: state => {
    let trainingSessionsSorted = {};
    let keysOrdered = Object.keys(state.training_sessions);

    console.log("state.direction is: ", state.direction);
    console.log("state.sort is: ", state.sort);

    keysOrdered.sort((a, b) => {
      let trainingSessionAProp = state.training_sessions[a][
        state.sort
      ].toLowerCase();
      let trainingSessionBProp = state.training_sessions[b][
        state.sort
      ].toLowerCase();

      // Check the added property 'direction' for 'newest' or 'direction' and if true direction the list.
      if (state.sort === "date" && state.direction === "descending") {
        if (trainingSessionAProp > trainingSessionBProp) return 1;
        else if (trainingSessionAProp < trainingSessionBProp) return -1;
        else return 0;
      } else if (state.sort === "title" && state.direction === "descending") {
        if (trainingSessionAProp < trainingSessionBProp) return -1;
        else if (trainingSessionAProp > trainingSessionBProp) return 1;
        else return 0;
      } else if (state.sort === "title" && state.direction === "ascending") {
        if (trainingSessionAProp < trainingSessionBProp) return 1;
        else if (trainingSessionAProp > trainingSessionBProp) return -1;
        else return 0;
      }

      if (trainingSessionAProp < trainingSessionBProp) return 1;
      else if (trainingSessionAProp > trainingSessionBProp) return -1;
      else return 0;
    });

    keysOrdered.forEach(key => {
      trainingSessionsSorted[key] = state.training_sessions[key];
    });

    return trainingSessionsSorted;
  },
  trainingSessionsFiltered: (state, getters) => {
    let trainingSessionsSorted = getters.trainingSessionsSorted;
    let trainingSessionsFiltered = {};
    if (state.search) {
      Object.keys(trainingSessionsSorted).forEach(function(key) {
        let trainingSession = trainingSessionsSorted[key];
        let trainingSessionLowerCase = trainingSession.title.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (trainingSessionLowerCase.includes(searchLowerCase)) {
          trainingSessionsFiltered[key] = trainingSession;
        }
      });
      return trainingSessionsFiltered;
    }
    return trainingSessionsSorted;
  },
  trainingSessions: (state, getters) => {
    let trainingSessionsFiltered = getters.trainingSessionsFiltered;
    let trainingSessions = {};
    Object.keys(trainingSessionsFiltered).forEach(function(key) {
      let trainingSession = trainingSessionsFiltered[key];
      trainingSessions[key] = trainingSession;
    });
    return trainingSessions;
  },
  trainingSessionsGi: (state, getters) => {
    let trainingSessionsFiltered = getters.trainingSessionsFiltered;
    let trainingSessions = {};
    Object.keys(trainingSessionsFiltered).forEach(function(key) {
      let trainingSession = trainingSessionsFiltered[key];
      if (trainingSession.type === "Gi")
        trainingSessions[key] = trainingSession;
    });
    return trainingSessions;
  },
  trainingSessionsNoGi: (state, getters) => {
    let trainingSessionsFiltered = getters.trainingSessionsFiltered;
    let trainingSessions = {};
    Object.keys(trainingSessionsFiltered).forEach(function(key) {
      let trainingSession = trainingSessionsFiltered[key];
      if (trainingSession.type === "No-Gi")
        trainingSessions[key] = trainingSession;
    });
    return trainingSessions;
  },
  trainingSessionsNoType: (state, getters) => {
    let trainingSessionsFiltered = getters.trainingSessionsFiltered;
    let trainingSessions = {};
    Object.keys(trainingSessionsFiltered).forEach(function(key) {
      let trainingSession = trainingSessionsFiltered[key];
      if (trainingSession.type === "") trainingSessions[key] = trainingSession;
    });
    return trainingSessions;
  },
  trainingSessionsFilmStudy: (state, getters) => {
    let trainingSessionsFiltered = getters.trainingSessionsFiltered;
    let trainingSessions = {};
    Object.keys(trainingSessionsFiltered).forEach(function(key) {
      let trainingSession = trainingSessionsFiltered[key];
      if (trainingSession.type === "Film Study")
        trainingSessions[key] = trainingSession;
    });
    return trainingSessions;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
