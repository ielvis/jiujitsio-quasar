// // NOTE: Need to import Vue to use Vue methods to delete
import { LocalStorage } from "quasar";

const state = {
  settings: {
    show12HourTimeFormat: true,
    beltColor: "",
    stripes: null
  }
};

// NOTE: Cannot be asynchronous
const mutations = {
  setShow12HourTimeFormat(state, value) {
    state.settings.show12HourTimeFormat = value;
  },
  setBeltColor(state, value) {
    state.settings.beltColor = value;
  },
  setNumberOfStripes(state, value) {
    state.settings.stripes = value;
  },
  setSettings(state, settings) {
    Object.assign(state.settings, settings);
  }
};

// NOTE: Can be asynchronous
const actions = {
  setShow12HourTimeFormat({ commit, dispatch }, value) {
    commit("setShow12HourTimeFormat", value);
    // NOTE: 'dispatch' used here to save settings to Local Storage
    dispatch("saveSettings");
  },
  setBeltColor({ commit, dispatch }, value) {
    commit("setBeltColor", value);
    dispatch("saveSettings");
  },
  setNumberOfStripes({ commit, dispatch }, value) {
    commit("setNumberOfStripes", value);
    dispatch("saveSettings");
  },
  saveSettings({ state }) {
    // NOTE: Use Quasar's LocalStorage plugin to persist settings
    LocalStorage.set("settings", state.settings);
  },
  // NOTE: gets settings from Local Storage when App fires up if there are any
  getSettings({ commit }) {
    let settings = LocalStorage.getItem("settings");
    if (settings) {
      commit("setSettings", settings);
    }
  }
};

const getters = {
  settings: state => {
    return state.settings;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
