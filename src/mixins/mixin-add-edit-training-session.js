export default {
  methods: {
    addTechniqueDetailedNote() {
      this.$emit("add-detailed-technique-note");
    },
    submitTrainingSessionForm() {
      // NOTE: This was updated from simply being $refs.title w/the intro of
      // NOTE: the re-usable component.
      this.$refs.modalTitle.$refs.title.validate();
      // An error gets thrown if the field is not valid
      if (!this.$refs.modalTitle.$refs.title.hasError) {
        this.submitTrainingSession();
      }
    }
  },
  components: {
    "modal-header": require("components/TrainingSessions/Modals/Shared/ModalHeader.vue")
      .default,
    "modal-title": require("components/TrainingSessions/Modals/Shared/ModalTitle.vue")
      .default,
    "modal-button": require("components/TrainingSessions/Modals/Shared/ModalButton.vue")
      .default
  }
};
