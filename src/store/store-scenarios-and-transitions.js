// NOTE: Need to import Vue to use Vue methods to delete
import Vue from "vue";
import { uid } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "../functions/function-show-error-message";
import { Notify } from "quasar";

const state = {
  scenarios_and_transitions: {
    scenarios: {},
    transitions: {},
    newScenarioId: "",
    newTransitionId: "",
    previousScenarioId: "",
    previousTransitionId: ""
  },
  search: "",
  sort: "name",
  show: "all",
  scenariosDownloaded: false,
  transitionsDownloaded: false
};

// NOTE: Cannot be asynchronous
const mutations = {
  addScenario(state, payload) {
    Vue.set(state.scenarios_and_transitions.scenarios, payload.id, payload);
    if (!state.scenarios_and_transitions.newScenarioId.length) {
      state.scenarios_and_transitions.previousScenarioId = payload.scenarioId;
      state.scenarios_and_transitions.previousTransitionId = payload.previousTransitionId;
    }
    state.scenarios_and_transitions.newScenarioId = payload.id;
    state.scenarios_and_transitions.newTransitionId = payload.newTransitionId;
  },
  deleteScenario(state, id) {
    Vue.delete(state.scenarios_and_transitions.scenarios, id);
  },
  deleteTransition(state, id) {
    Vue.delete(state.scenarios_and_transitions.transitions, id);
  },
  addTransition(state, payload) {
    Vue.set(state.scenarios_and_transitions.transitions, payload.id, payload);
  },
  updatePreviousScenario(state, id) {
    state.scenarios_and_transitions.previousScenarioId = id;
  },
  updateNewScenarioId(state, id) {
    state.scenarios_and_transitions.newScenarioId = id;
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setScenariosDownloaded(state, value) {
    state.scenariosDownloaded = value;
  },
  setTransitionsDownloaded(state, value) {
    state.transitionsDownloaded = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  firebaseReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("scenarios")
      .get()
      .then(response => {
        // console.log("scenario get response is: ", response);
        commit("setScenariosDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    // NOTE: Scenario and/or Transition Added, Edited or Removed 🤯
    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("scenarios")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();
          // console.log("payload get response is: ", payload);
          if (change.type === "added") {
            // console.log("in added, payload is: ", payload);
            commit("addScenario", payload);
          } else if (change.type === "modified") {
            commit("updateScenario", payload);
          } else if (change.type === "removed") {
            commit("deleteScenario", payload.id);
          }
        });
      });

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("transitions")
      .get()
      .then(response => {
        commit("setTransitionsDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("transitions")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();
          if (change.type === "added") {
            commit("addTransition", payload);
          } else if (change.type === "modified") {
            commit("updateTransition", payload);
          } else if (change.type === "removed") {
            commit("deleteTransition", payload.id);
          }
        });
      });
  },
  addScenario({ dispatch }, scenario) {
    let scenarioId = scenario.id;
    let previousTransitionId = scenario.transtionInId;

    let payload = {
      id: scenarioId,
      scenario: scenario, 
      previousTransitionId: previousTransitionId
    };
    // console.log("in addScenario, payload is: ", payload);
    dispatch("firebaseAddScenario", payload);
  },
  deleteScenario({ dispatch }, id) {
    dispatch("firebaseDeleteScenario", id);
  },
  // NOTE: This is with the firebase db
  firebaseDeleteScenario({}, scenario) {
    let userId = firebaseAuth.currentUser.uid;

    console.log("In firebaseDeleteScenario SCENARIO IS: ", scenario)
    
    // * Delete scenario from firebase collection 
    var scenarioRef = firebaseDb
    .collection("users")
    .doc(userId)
    .collection("scenarios")
    .doc(scenario.id);
    
    scenarioRef
    .delete()
    .then(function() {
      Notify.create("Scenario deleted!");
    })
    .catch(function(error) {
      console.log("Error deleting a SCENARIO! error.message is: ", error.message)
      showErrorMessage(error.message);
    });

    // * Delete transition from firebase collection 
    if (scenario.previousTransitionId) {
      console.log("TRYING TO DELETE A TRANSITION")
      console.log("scenario.previousTransitionId", scenario.previousTransitionId)
      var transitionRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("transitions")
      .doc(scenario.previousTransitionId);
      
      transitionRef
      .delete()
      .then(function() {
        Notify.create("Transition deleted!");
      })
      .catch(function(error) {
        console.log("error.message is: ", error.message)
        showErrorMessage(error.message);
      });
    }

  },
  deleteTransition({ commit }, id) {
    commit("deleteTransition", id);
  },
  firebaseAddScenario({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("scenarios")
      .doc(payload.id)
      .set(payload.scenario)
      .then(function() {
        // Notify.create("Scenario added!");
        // console.log("Scenario added (payload): ", payload);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  addTransition({ dispatch }, transition) {
    let transitionId = transition.id;
    let payload = {
      id: transitionId,
      transition: transition
    };
    dispatch("firebaseAddTransition", payload);
  },
  firebaseAddTransition({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("transitions")
      .doc(payload.id)
      .set(payload.transition)
      .then(function() {
        // Notify.create("Transition added!");
        // console.log("Transition added (payload): ", payload);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  updatePreviousScenario({ commit }, id) {
    commit("updatePreviousScenario", id);
  },
  updateNewScenarioId({ commit }, id) {
    commit("updateNewScenarioId", id);
  }
};

const getters = {
  scenariosSorted: state => {
    let scenariosSorted = {};
    let keysOrdered = Object.keys(state.scenarios_and_transitions.scenarios);

    // console.log("keysOrdered are: ", keysOrdered);

    // console.log(
    //   "state.scenarios_and_transitions",
    //   state.scenarios_and_transitions
    // );

    keysOrdered.sort((a, b) => {
      let scenarioAProp = state.scenarios_and_transitions.scenarios[a][
        state.sort
      ].toLowerCase();
      let scenarioBProp = state.scenarios_and_transitions.scenarios[b][
        state.sort
      ].toLowerCase();

      if (scenarioAProp > scenarioBProp) return 1;
      else if (scenarioAProp < scenarioBProp) return -1;
      else return 0;
    });

    keysOrdered.forEach(key => {
      // console.log("key is: ", key);
      // console.log(
      //   "state.scenarios_and_transitions: ",
      //   state.scenarios_and_transitions
      // );
      // console.log(
      //   "state.scenarios_and_transitions.scenarios: ",
      //   state.scenarios_and_transitions.scenarios
      // );
      scenariosSorted[key] = state.scenarios_and_transitions.scenarios[key];
    });

    // console.log("scenariosSorted are: ", scenariosSorted);
    return scenariosSorted;
  },
  scenariosFiltered: (state, getters) => {
    let scenariosSorted = getters.scenariosSorted;
    let scenariosFiltered = {};
    if (state.search) {
      Object.keys(scenariosSorted).forEach(function(key) {
        let scenario = scenariosSorted[key];
        let scenarioLowerCase = scenario.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (scenarioLowerCase.includes(searchLowerCase)) {
          scenariosFiltered[key] = scenario;
        }
      });
      return scenariosFiltered;
    }
    return scenariosSorted;
  },
  scenarios: (state, getters) => {
    let scenariosFiltered = getters.scenariosFiltered;
    let scenarios = {};
    Object.keys(scenariosFiltered).forEach(function(key) {
      let scenario = scenariosFiltered[key];
      scenarios[key] = scenario;
    });
    return scenarios;
  },
  transitionsSorted: state => {
    let transitionsSorted = {};
    let keysOrdered = Object.keys(state.scenarios_and_transitions.transitions);

    keysOrdered.sort((a, b) => {
      let transitionAProp = state.scenarios_and_transitions.transitions[a][
        state.sort
      ].toLowerCase();
      let transitionBprop = state.scenarios_and_transitions.transitions[b][
        state.sort
      ].toLowerCase();

      if (transitionAProp > transitionBprop) return 1;
      else if (transitionAProp < transitionBprop) return -1;
      else return 0;
    });

    keysOrdered.forEach(key => {
      transitionsSorted[key] = state.scenarios_and_transitions.transitions[key];
    });

    return transitionsSorted;
  },
  transitionsFiltered: (state, getters) => {
    let transitionsSorted = getters.transitionsSorted;
    let transitionsFiltered = {};
    if (state.search) {
      Object.keys(transitionsSorted).forEach(function(key) {
        let transition = transitionsSorted[key];
        let transitionLowerCase = transition.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (transitionLowerCase.includes(searchLowerCase)) {
          transitionsFiltered[key] = transition;
        }
      });
      return transitionsFiltered;
    }
    return transitionsSorted;
  },
  transitions: (state, getters) => {
    let transitionsFiltered = getters.transitionsFiltered;
    let transitions = {};
    Object.keys(transitionsFiltered).forEach(function(key) {
      let transition = transitionsFiltered[key];
      transitions[key] = transition;
    });
    return transitions;
  },
  newScenarioId: state => {
    return state.scenarios_and_transitions.newScenarioId;
  },
  previousScenarioId: state => {
    return state.scenarios_and_transitions.previousScenarioId;
  },
  previousTransitionId: state => {
    return state.scenarios_and_transitions.previousTransitionId;
  },
  scenariosByTrainingSessionId: (state, getters) => id => {
    let allScenarios = getters.scenarios;
    let matchingScenarios = {};
    Object.keys(allScenarios).forEach(function(key) {
      let scenario = allScenarios[key];
      if (scenario.id === id) matchingScenarios[key] = scenario;
    });
    return matchingScenarios;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
