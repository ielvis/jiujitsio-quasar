import Vue from "vue";
import Vuex from "vuex";

// import example from './module-example'
import auth from "./store-auth";
import scenarios_and_transitions from "./store-scenarios-and-transitions";
import settings from "./store-settings";
import techniques from "./store-techniques";
import partners from "./store-partners";
import gyms from "./store-gyms";
import instructors from "./store-instructors";



import training_sessions from "./store-training-sessions";
import VueTimeline from "@growthbunker/vuetimeline";

Vue.use(Vuex);
Vue.use(VueTimeline);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function(/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      // example
      auth,
      scenarios_and_transitions,
      settings,
      partners,
      gyms,
      instructors,
      techniques,
      training_sessions
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  });

  return Store;
}
