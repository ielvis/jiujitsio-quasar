const routes = [
  {
    path: "/",
    component: () => import("layouts/Layout.vue"),
    children: [
      { path: "", component: () => import("pages/PageTrainingSessions.vue") },
      {
        path: "/devJournal",
        component: () => import("pages/PageDevJournal.vue")
      },

      {
        path: "/techniques-map",
        component: () => import("pages/PageTechniquesMap.vue")
      },
      {
        path: "/statistics",
        component: () => import("pages/PageStatistics.vue")
      },
      { path: "/store", component: () => import("pages/PageStore.vue") },
      { path: "/settings", component: () => import("pages/PageSettings.vue") },
      { path: "/settings/help", component: () => import("pages/PageHelp.vue") },
      { path: "/auth", component: () => import("pages/PageAuth.vue") }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
