// Need to import Vue to use Vue methods to delete
import Vue from "vue";
import { uid } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "../functions/function-show-error-message";
import { Notify } from "quasar";

const state = {
  techniques: {
    //   "ID1": {
    //   name: "B Open Mat Sunday",
    //   type: "Gi",
    //   general_notes: "Was fun.",
    //   name: "a"
    // },
    // "ID2": {
    //   name: "A Open Mat Saturday",
    //   type: "NoGi",
    //   general_notes: "Was TEst.",
    //   name: "b"
    // }
  },
  search: "",
  sort: "name",
  show: "all",
  techniquesDownloaded: false,
  direction: "ascending"
};

// Cannot be asynchronous
const mutations = {
  // Use Object.assign to target a property and apply the payload updates.
  updateTechnique(state, payload) {
    // NOTE: Taking care of payload.updates on the Firestore method
    Object.assign(state.techniques[payload.id], payload);
  },
  deleteTechnique(state, id) {
    Vue.delete(state.techniques, id);
  },
  addTechnique(state, payload) {
    Vue.set(state.techniques, payload.id, payload);
  },
  clearTechniques(state) {
    state.techniques = {};
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setDirection(state, value) {
    state.direction = value;
  },
  setShow(state, value) {
    state.show = value;
  },
  setTechniquesDownloaded(state, value) {
    state.techniquesDownloaded = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  updateTechnique({ dispatch }, payload) {
    dispatch("firebaseUpdateTechnique", payload);
  },
  // NOTE: Pass a commit method and the payload(ID of the technique in this case)
  // NOTE: to the action method, commit the payload to a mutation to change state.
  deleteTechnique({ dispatch }, id) {
    dispatch("firebaseDeleteTechnique", id);
  },
  // NOTE: Now that the Firestore has been added we can now dispatch an action to update the
  // NOTE: Firestore database. Our listener will update the state automatically
  // addTechnique({ commit }, technique) {
  addTechnique({ dispatch }, technique) {
    // NOTE, id being set in parent training session
    // let techniqueId = uid();
    // technique.id = techniqueId;
    let payload = technique;
    // commit("addTechnique", payload);
    dispatch("firebaseAddTechnique", payload);
  },
  setSearch({ commit }, value) {
    commit("setSearch", value);
  },
  setSort({ commit }, value) {
    commit("setSort", value);
  },
  setDirection({ commit }, value) {
    commit("setDirection", value);
  },
  setShow({ commit }, value) {
    commit("setShow", value);
  },
  firebaseReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("techniques")
      .get()
      .then(response => {
        commit("setTechniquesDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    // NOTE: Training Session Added, Edited or Removed 🤯
    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("techniques")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();

          if (change.type === "added") {
            commit("addTechnique", payload);
          } else if (change.type === "modified") {
            commit("updateTechnique", payload);
          } else if (change.type === "removed") {
            commit("deleteTechnique", payload.id);
          }
        });
      });
  },
  firebaseAddTechnique({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("techniques")
      .doc(payload.id)
      .set(payload)
      .then(function() {
        Notify.create("Technique added!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  firebaseUpdateTechnique({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    var techniqueRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("techniques")
      .doc(payload.id);

    techniqueRef
      .update(payload.updates)
      .then(function() {
        Notify.create("Technique updated!");
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        showErrorMessage(error.message);
      });
  },
  firebaseDeleteTechnique({}, techniqueId) {
    let userId = firebaseAuth.currentUser.uid;

    var techniqueRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("techniques")
      .doc(techniqueId);

    techniqueRef
      .delete()
      .then(function() {
        Notify.create("Technique deleted!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  }
};

const getters = {
  techniquesSorted: state => {
    let techniquesSorted = {};
    let keysOrdered = Object.keys(state.techniques);

    console.log("state.direction is: ", state.direction);
    console.log("state.sort is: ", state.sort);

    keysOrdered.sort((a, b) => {
      console.log("state.techniques is: ", state.techniques);
      let techniqueAProp = state.techniques[a][state.sort].toLowerCase();
      let techniqueBProp = state.techniques[b][state.sort].toLowerCase();

      // Check the added property 'direction' for 'newest' or 'direction' and if true direction the list.
      if (state.sort === "name" && state.direction === "ascending") {
        if (techniqueAProp > techniqueBProp) return 1;
        else if (techniqueAProp < techniqueBProp) return -1;
        else return 0;
      }
    });

    keysOrdered.forEach(key => {
      techniquesSorted[key] = state.techniques[key];
    });

    return techniquesSorted;
  },
  techniquesFiltered: (state, getters) => {
    let techniquesSorted = getters.techniquesSorted;
    let techniquesFiltered = {};
    if (state.search) {
      Object.keys(techniquesSorted).forEach(function(key) {
        let technique = techniquesSorted[key];
        let techniqueLowerCase = technique.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (techniqueLowerCase.includes(searchLowerCase)) {
          techniquesFiltered[key] = technique;
        }
      });
      return techniquesFiltered;
    }
    return techniquesSorted;
  },
  allTechniques: (state, getters) => {
    console.log("HELLOOOOOO");

    let techniquesFiltered = getters.techniquesFiltered;
    let techniques = {};
    Object.keys(techniquesFiltered).forEach(function(key) {
      let technique = techniquesFiltered[key];
      techniques[key] = technique;
    });
    console.log("techniques are: ", techniques);
    return techniques;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
