// Need to import Vue to use Vue methods to delete
import Vue from "vue";
import { uid } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "../functions/function-show-error-message";
import { Notify } from "quasar";

const state = {
  instructors: {
    //   "ID1": {
    //   name: "B Open Mat Sunday",
    //   type: "Gi",
    //   general_notes: "Was fun.",
    //   name: "a"
    // },
    // "ID2": {
    //   name: "A Open Mat Saturday",
    //   type: "NoGi",
    //   general_notes: "Was TEst.",
    //   name: "b"
    // }
  },
  search: "",
  sort: "name",
  show: "all",
  instructorsDownloaded: false,
  direction: "ascending"
};

// Cannot be asynchronous
const mutations = {
  // Use Object.assign to target a property and apply the payload updates.
  updateInstructor(state, payload) {
    // NOTE: Taking care of payload.updates on the Firestore method
    Object.assign(state.instructors[payload.id], payload);
  },
  deleteInstructor(state, id) {
    Vue.delete(state.instructors, id);
  },
  addInstructor(state, payload) {
    Vue.set(state.instructors, payload.id, payload);
  },
  clearInstructors(state) {
    state.instructors = {};
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setDirection(state, value) {
    state.direction = value;
  },
  setShow(state, value) {
    state.show = value;
  },
  setInstructorsDownloaded(state, value) {
    state.instructorsDownloaded = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  updateInstructor({ dispatch }, payload) {
    dispatch("firebaseUpdateInstructor", payload);
  },
  // NOTE: Pass a commit method and the payload(ID of the instructor in this case)
  // NOTE: to the action method, commit the payload to a mutation to change state.
  deleteInstructor({ dispatch }, id) {
    dispatch("firebaseDeleteInstructor", id);
  },
  // NOTE: Now that the Firestore has been added we can now dispatch an action to update the
  // NOTE: Firestore database. Our listener will update the state automatically
  // addInstructor({ commit }, instructor) {
  addInstructor({ dispatch }, instructor) {
    // NOTE, id being set in parent training session
    // let instructorId = uid();
    // instructor.id = instructorId;
    let payload = instructor;
    // commit("addInstructor", payload);
    dispatch("firebaseAddInstructor", payload);
  },
  setSearch({ commit }, value) {
    commit("setSearch", value);
  },
  setSort({ commit }, value) {
    commit("setSort", value);
  },
  setDirection({ commit }, value) {
    commit("setDirection", value);
  },
  setShow({ commit }, value) {
    commit("setShow", value);
  },
  firebaseReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("instructors")
      .get()
      .then(response => {
        commit("setInstructorsDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    // NOTE: Training Session Added, Edited or Removed 🤯
    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("instructors")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();

          if (change.type === "added") {
            commit("addInstructor", payload);
          } else if (change.type === "modified") {
            commit("updateInstructor", payload);
          } else if (change.type === "removed") {
            commit("deleteInstructor", payload.id);
          }
        });
      });
  },
  firebaseAddInstructor({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("instructors")
      .doc(payload.id)
      .set(payload)
      .then(function() {
        Notify.create("Instructor added!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  firebaseUpdateInstructor({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    var instructorRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("instructors")
      .doc(payload.id);

    instructorRef
      .update(payload.updates)
      .then(function() {
        Notify.create("Instructor updated!");
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        showErrorMessage(error.message);
      });
  },
  firebaseDeleteInstructor({}, instructorId) {
    let userId = firebaseAuth.currentUser.uid;

    var instructorRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("instructors")
      .doc(instructorId);

    instructorRef
      .delete()
      .then(function() {
        Notify.create("Instructor deleted!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  }
};

const getters = {
  instructorsSorted: state => {
    let instructorsSorted = {};
    let keysOrdered = Object.keys(state.instructors);

    console.log("state.direction is: ", state.direction);
    console.log("state.sort is: ", state.sort);

    keysOrdered.sort((a, b) => {
      let instructorAProp = state.instructors[a][
        state.sort
      ].toLowerCase();
      let instructorBProp = state.instructors[b][
        state.sort
      ].toLowerCase();

      // Check the added property 'direction' for 'newest' or 'direction' and if true direction the list.
      if (state.sort === "name" && state.direction === "ascending") {
        if (instructorAProp > instructorBProp) return 1;
        else if (instructorAProp < instructorBProp) return -1;
        else return 0;
      } 
    });

    keysOrdered.forEach(key => {
      instructorsSorted[key] = state.instructors[key];
    });

    return instructorsSorted;
  },
  instructorsFiltered: (state, getters) => {
    let instructorsSorted = getters.instructorsSorted;
    let instructorsFiltered = {};
    if (state.search) {
      Object.keys(instructorsSorted).forEach(function(key) {
        let instructor = instructorsSorted[key];
        let instructorLowerCase = instructor.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (instructorLowerCase.includes(searchLowerCase)) {
          instructorsFiltered[key] = instructor;
        }
      });
      return instructorsFiltered;
    }
    return instructorsSorted;
  },
  allInstructors: (state, getters) => {
    let instructorsFiltered = getters.instructorsFiltered;
    let instructors = {};
    Object.keys(instructorsFiltered).forEach(function(key) {
      let instructor = instructorsFiltered[key];
      instructors[key] = instructor;
    });
    return instructors;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
