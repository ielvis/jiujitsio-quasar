// Need to import Vue to use Vue methods to delete
import Vue from "vue";
import { uid } from "quasar";
import { firebaseDb, firebaseAuth } from "boot/firebase";
import { showErrorMessage } from "../functions/function-show-error-message";
import { Notify } from "quasar";

const state = {
  partners: {
    //   "ID1": {
    //   name: "B Open Mat Sunday",
    //   type: "Gi",
    //   general_notes: "Was fun.",
    //   name: "a"
    // },
    // "ID2": {
    //   name: "A Open Mat Saturday",
    //   type: "NoGi",
    //   general_notes: "Was TEst.",
    //   name: "b"
    // }
  },
  search: "",
  sort: "name",
  show: "all",
  partnersDownloaded: false,
  direction: "ascending"
};

// Cannot be asynchronous
const mutations = {
  // Use Object.assign to target a property and apply the payload updates.
  updatePartner(state, payload) {
    // NOTE: Taking care of payload.updates on the Firestore method
    Object.assign(state.partners[payload.id], payload);
  },
  deletePartner(state, id) {
    Vue.delete(state.partners, id);
  },
  addPartner(state, payload) {
    Vue.set(state.partners, payload.id, payload);
  },
  clearPartners(state) {
    state.partners = {};
  },
  setSearch(state, value) {
    state.search = value;
  },
  setSort(state, value) {
    state.sort = value;
  },
  setDirection(state, value) {
    state.direction = value;
  },
  setShow(state, value) {
    state.show = value;
  },
  setPartnersDownloaded(state, value) {
    state.partnersDownloaded = value;
  }
};

// NOTE: Can be asynchronous
const actions = {
  updatePartner({ dispatch }, payload) {
    dispatch("firebaseUpdatePartner", payload);
  },
  // NOTE: Pass a commit method and the payload(ID of the partner in this case)
  // NOTE: to the action method, commit the payload to a mutation to change state.
  deletePartner({ dispatch }, id) {
    dispatch("firebaseDeletePartner", id);
  },
  // NOTE: Now that the Firestore has been added we can now dispatch an action to update the
  // NOTE: Firestore database. Our listener will update the state automatically
  // addPartner({ commit }, partner) {
  addPartner({ dispatch }, partner) {
    // NOTE, id being set in parent training session
    // let partnerId = uid();
    // partner.id = partnerId;
    let payload = partner;
    // commit("addPartner", payload);
    dispatch("firebaseAddPartner", payload);
  },
  setSearch({ commit }, value) {
    commit("setSearch", value);
  },
  setSort({ commit }, value) {
    commit("setSort", value);
  },
  setDirection({ commit }, value) {
    commit("setDirection", value);
  },
  setShow({ commit }, value) {
    commit("setShow", value);
  },
  firebaseReadData({ commit }) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("partners")
      .get()
      .then(response => {
        commit("setPartnersDownloaded", true);
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });

    // NOTE: Training Session Added, Edited or Removed 🤯
    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("partners")
      .onSnapshot(function(snapshot) {
        snapshot.docChanges().forEach(function(change) {
          let payload = change.doc.data();

          if (change.type === "added") {
            commit("addPartner", payload);
          } else if (change.type === "modified") {
            commit("updatePartner", payload);
          } else if (change.type === "removed") {
            commit("deletePartner", payload.id);
          }
        });
      });
  },
  firebaseAddPartner({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    firebaseDb
      .collection("users")
      .doc(userId)
      .collection("partners")
      .doc(payload.id)
      .set(payload)
      .then(function() {
        Notify.create("Partner added!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  },
  firebaseUpdatePartner({}, payload) {
    let userId = firebaseAuth.currentUser.uid;

    var partnerRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("partners")
      .doc(payload.id);

    partnerRef
      .update(payload.updates)
      .then(function() {
        Notify.create("Partner updated!");
      })
      .catch(function(error) {
        // The document probably doesn't exist.
        showErrorMessage(error.message);
      });
  },
  firebaseDeletePartner({}, partnerId) {
    let userId = firebaseAuth.currentUser.uid;

    var partnerRef = firebaseDb
      .collection("users")
      .doc(userId)
      .collection("partners")
      .doc(partnerId);

    partnerRef
      .delete()
      .then(function() {
        Notify.create("Partner deleted!");
      })
      .catch(function(error) {
        showErrorMessage(error.message);
      });
  }
};

const getters = {
  partnersSorted: state => {
    let partnersSorted = {};
    let keysOrdered = Object.keys(state.partners);

    console.log("state.direction is: ", state.direction);
    console.log("state.sort is: ", state.sort);

    keysOrdered.sort((a, b) => {
      let partnerAProp = state.partners[a][
        state.sort
      ].toLowerCase();
      let partnerBProp = state.partners[b][
        state.sort
      ].toLowerCase();

      // Check the added property 'direction' for 'newest' or 'direction' and if true direction the list.
      if (state.sort === "name" && state.direction === "ascending") {
        if (partnerAProp > partnerBProp) return 1;
        else if (partnerAProp < partnerBProp) return -1;
        else return 0;
      } 
    });

    keysOrdered.forEach(key => {
      partnersSorted[key] = state.partners[key];
    });

    return partnersSorted;
  },
  partnersFiltered: (state, getters) => {
    let partnersSorted = getters.partnersSorted;
    let partnersFiltered = {};
    if (state.search) {
      Object.keys(partnersSorted).forEach(function(key) {
        let partner = partnersSorted[key];
        let partnerLowerCase = partner.name.toLowerCase();
        let searchLowerCase = state.search.toLowerCase();
        if (partnerLowerCase.includes(searchLowerCase)) {
          partnersFiltered[key] = partner;
        }
      });
      return partnersFiltered;
    }
    return partnersSorted;
  },
  allPartners: (state, getters) => {
    let partnersFiltered = getters.partnersFiltered;
    let partners = {};
    Object.keys(partnersFiltered).forEach(function(key) {
      let partner = partnersFiltered[key];
      partners[key] = partner;
    });
    return partners;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
